import 'package:test/test.dart';
import 'package:unique_instance/unique_instance.dart';

class Exemple {
  final int a;
  final int b;
  Exemple(this.a, this.b);
}

void main() {

  group("test register", () {

    final Exemple exemple = Exemple(2, 3);

    test("contains instance", () {
      final UniqueInstance instance = UniqueInstance.getInstance();
      instance.register<Exemple>(exemple);
      expect(instance.contains<Exemple>(), true);
    });

    test("get register instance", () {
      final UniqueInstance instance = UniqueInstance.getInstance();
      expect(instance.get<Exemple>(), exemple);
    });

    test("get register instance", () {
      final UniqueInstance instance = UniqueInstance.getInstance();
      instance.delete<Exemple>();
      expect(instance.contains<Exemple>(), false);
    });

  });

  group("test lazy register", () {

  });

}

